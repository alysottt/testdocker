FROM php:7.2-apache

RUN touch /etc/apache2/sites-enabled/local.conf \
    && echo 'Include sites-enabled/local.conf' >> /etc/apache2/apache2.conf

COPY / /var/www/html/
RUN a2enmod rewrite \
 && a2enmod headers \
 && a2enmod deflate